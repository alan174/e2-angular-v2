import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'e2-link',
  templateUrl: './e2-link.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class E2LinkComponent implements OnInit {
  @Input() routerLink = '';
  @Input() link = '';
  @Input() linkTarget = '';
  @Input() linkText = '';

  constructor() {}

  ngOnInit(): void {}
}
