import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { E2LinkComponent } from './e2-link.component';

@NgModule({
  declarations: [E2LinkComponent],
  imports: [CommonModule],
  exports: [E2LinkComponent],
  entryComponents: [E2LinkComponent],
})
export class E2LinkModule {}
