import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { E2LinkComponent } from './e2-link.component';

describe('E2LinkComponent', () => {
  let component: E2LinkComponent;
  let fixture: ComponentFixture<E2LinkComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ E2LinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(E2LinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
