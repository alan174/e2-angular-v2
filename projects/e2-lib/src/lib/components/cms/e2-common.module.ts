import { NgModule } from '@angular/core';
import { E2LinkModule } from './e2-link/e2-link.module';
import { E2MediaModule } from './e2-media/e2-media.module';
import { E2ParagraphModule } from './e2-paragraph/e2-paragraph.module';

@NgModule({
  imports: [
    E2LinkModule,
    E2MediaModule,
    E2ParagraphModule,
  ],
  exports: [
    E2LinkModule,
    E2MediaModule,
    E2ParagraphModule,
  ]
})
export class E2CommonModule {}