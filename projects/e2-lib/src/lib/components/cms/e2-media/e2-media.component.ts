import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'e2-media',
  templateUrl: './e2-media.component.html',
})
export class E2MediaComponent implements OnChanges {
  @Input() container: any;
  media: any;

  constructor(  ) {}

  ngOnChanges(): void {
    this.media = {
      src : this.container?.url,
      alt : this.container?.altText,
    }
  }
}
