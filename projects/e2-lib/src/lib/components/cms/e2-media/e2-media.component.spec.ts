import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { E2MediaComponent } from './e2-media.component';

describe('E2MediaComponent', () => {
  let component: E2MediaComponent;
  let fixture: ComponentFixture<E2MediaComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ E2MediaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(E2MediaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
