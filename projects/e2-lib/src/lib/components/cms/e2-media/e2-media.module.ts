import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { E2MediaComponent } from './e2-media.component';

@NgModule({
  declarations: [E2MediaComponent],
  imports: [
    CommonModule,
  ],
  exports: [E2MediaComponent],
  entryComponents: [E2MediaComponent],
})
export class E2MediaModule {}
