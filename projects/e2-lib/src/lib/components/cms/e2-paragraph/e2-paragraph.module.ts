import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { E2ParagraphComponent } from './e2-paragraph.component';

@NgModule({
  declarations: [E2ParagraphComponent],
  imports: [CommonModule],
  exports: [E2ParagraphComponent],
  entryComponents: [E2ParagraphComponent],
})
export class E2ParagraphModule {}
