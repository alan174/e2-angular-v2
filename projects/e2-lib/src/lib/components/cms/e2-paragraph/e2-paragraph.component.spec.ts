import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { E2ParagraphComponent } from './e2-paragraph.component';

describe('E2ParagraphComponent', () => {
  let component: E2ParagraphComponent;
  let fixture: ComponentFixture<E2ParagraphComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ E2ParagraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(E2ParagraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
