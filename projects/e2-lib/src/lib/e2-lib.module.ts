import { NgModule } from '@angular/core';
import { E2CommonModule } from './components/cms/e2-common.module';

@NgModule({
  imports: [
    E2CommonModule,
  ],
})
export class E2LibModule { }
