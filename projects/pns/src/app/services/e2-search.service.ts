import { Injectable } from '@angular/core';
import {
  ActivatedRoute, Router
} from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class E2SearchService  {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) {} 
  
  getSearchQueryParams() {
    return this.route.snapshot.queryParamMap?.get('query');
  }

  searchQueryParams(queryText?: string) {
    this.router.navigate(['search'], {
      relativeTo: this.route,
      queryParams: { query: queryText },
      queryParamsHandling: 'merge',
    });
  }
}