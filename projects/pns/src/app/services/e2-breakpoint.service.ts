import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BreakpointObserverService } from './breakpoint-observer.service';

@Injectable({
  providedIn: 'root'
})

export class E2BreakpointService extends BreakpointObserverService {
  isMobile$ = this.breakpoint$.pipe(
    map(res => {
      return res === 'xs';
    })
  );
}