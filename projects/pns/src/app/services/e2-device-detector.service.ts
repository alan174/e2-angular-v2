import { Injectable } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';

@Injectable()

export class E2DeviceDetectorService {

  constructor(
    private deviceService: DeviceDetectorService,
  ) {} 

  public get isDeviceInfo$(): any {
    return this.deviceService.getDeviceInfo();
  }
  
  public get isMobiledevice$(): boolean {
    const isMobile$ = this.deviceService.isMobile();
    const isTablet$ = this.deviceService.isTablet();

    return isMobile$ || isTablet$;
  }

}
