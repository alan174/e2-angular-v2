import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
// import { E2MediaModule } from 'e2-lib';
import { SwiperModule } from 'swiper/angular';
import { E2AddToCartModule } from '../../cart/e2-add-to-cart/e2-add-to-cart.module';
// import { E2NotifyMeModule } from '../../cart/e2-notify-me/e2-notify-me.module';
import { E2BrandPromotionComponent } from './e2-brand-promotion.component';
import { E2MediaModule } from '../../../../../../e2-lib/src/lib/components/cms';

@NgModule({
  declarations: [E2BrandPromotionComponent],
  imports: [
    CommonModule,
    SwiperModule,
    E2AddToCartModule,
    // E2NotifyMeModule,
    E2MediaModule,
    RouterModule,
  ],
  exports: [E2BrandPromotionComponent],
})
export class E2BrandPromotionModule {}
