import {
  Component,
  ElementRef,
  HostListener,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import SwiperCore, { Pagination, SwiperOptions } from 'swiper';

SwiperCore.use([Pagination]);
@Component({
  selector: 'e2-brand-promotion',
  templateUrl: './e2-brand-promotion.component.html',
})
export class E2BrandPromotionComponent implements OnInit {
  needFiller: any;
  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.needFiller = !(
      this.elementRef.nativeElement.closest('body')?.clientWidth < 861
    );
  }
  constructor(private elementRef: ElementRef) {}
  @Input() brandData: any;
  sectionClassName: any;
  config: SwiperOptions = {
    slidesPerView: 1,
    slidesPerGroup: 1,
    spaceBetween: 5,
    breakpoints: {
      768: {
        slidesPerView: 2,
        slidesPerGroup: 2,
      },
      861: {
        slidesPerView: 4,
        slidesPerGroup: 4,
      },
    },
  };

  @ViewChild('swiper') swiper: any;
  ngOnInit(): void {
    this.needFiller = !(
      this.elementRef.nativeElement.closest('body')?.clientWidth < 861
    );
    this.sectionClassName =
      this.brandData.name?.toLowerCase().split(/\W+/).join('-') || '';
    this.config = {
      ...this.config,
      navigation: {
        prevEl: 'e2-brand-promotion .' + this.sectionClassName + ' .previous',
        nextEl: 'e2-brand-promotion .' + this.sectionClassName + ' .next',
      },
      pagination: {
        el: 'e2-brand-promotion .' + this.sectionClassName + ' .pageNumber',
        type: 'fraction',
      },
    };
    this.swiper?.update();
  }
}
