import { ComponentFixture, TestBed } from '@angular/core/testing';

import { E2BrandPromotionComponent } from './e2-brand-promotion.component';

describe('E2BrandPromotionComponent', () => {
  let component: E2BrandPromotionComponent;
  let fixture: ComponentFixture<E2BrandPromotionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ E2BrandPromotionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(E2BrandPromotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
