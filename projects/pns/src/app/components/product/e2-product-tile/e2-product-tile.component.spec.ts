import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { E2ProductTileComponent } from './e2-product-tile.component';

describe('E2ProductTileComponent', () => {
  let component: E2ProductTileComponent;
  let fixture: ComponentFixture<E2ProductTileComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ E2ProductTileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(E2ProductTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
