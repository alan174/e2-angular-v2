import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { E2MediaModule } from 'e2-lib';
import { E2AddToCartModule } from '../../cart/e2-add-to-cart/e2-add-to-cart.module';
import { E2NotifyMeModule } from '../../cart/e2-notify-me/e2-notify-me.module';
import { E2AddToWishlistModule } from '../e2-add-to-wishlist/e2-add-to-wishlist.module';
import { E2ProductTileComponent } from './e2-product-tile.component';

@NgModule({
  declarations: [E2ProductTileComponent],
  imports: [
    CommonModule,
    E2AddToWishlistModule,
    E2AddToCartModule,
    E2NotifyMeModule,
    E2MediaModule,
    RouterModule,
  ],
  exports: [E2ProductTileComponent],
})
export class E2ProductTileModule {}
