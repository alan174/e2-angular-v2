import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'e2-product-tile',
  templateUrl: './e2-product-tile.component.html',
})
export class E2ProductTileComponent implements OnInit {

  @Input() product;
  constructor() { }

  ngOnInit(): void {
      this.product.image = this.product.image || 'https://fakeimg.pl/200x200/afbdea?text=NoImg';
  }

  checkHasStock():boolean{    
    return this.product?.hasStock != 'OUTOFSTOCK';
  }
}
