import { Component, Input } from '@angular/core';
import { SwiperOptions } from 'swiper';

@Component({
  selector: 'e2-product-preview',
  templateUrl: './e2-product-preview.component.html',
})
export class E2ProductPreviewComponent {
  @Input() previewList;
  config: SwiperOptions = {
    slidesPerView: 'auto',
    preventClicks: false,
    preventClicksPropagation: false,
    lazy: {
      loadPrevNext: true,
      loadPrevNextAmount: 3,
    },
    observer: true,
    preventInteractionOnTransition: true,
  };
}
