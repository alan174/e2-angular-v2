import { ComponentFixture, TestBed } from '@angular/core/testing';

import { E2ProductPreviewComponent } from './e2-product-preview.component';

describe('E2ProductPreviewComponent', () => {
  let component: E2ProductPreviewComponent;
  let fixture: ComponentFixture<E2ProductPreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ E2ProductPreviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(E2ProductPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
