import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
// import { E2MediaModule } from 'e2-lib';
import { SwiperModule } from 'swiper/angular';
import { E2AddToCartModule } from '../../cart/e2-add-to-cart/e2-add-to-cart.module';
// import { E2NotifyMeModule } from '../../cart/e2-notify-me/e2-notify-me.module';
import { E2ProductPreviewComponent } from './e2-product-preview.component';

@NgModule({
  declarations: [E2ProductPreviewComponent],
  imports: [
    CommonModule,
    SwiperModule,
    E2AddToCartModule,
    // E2NotifyMeModule,
    // E2MediaModule,
    RouterModule,
  ],
  exports: [E2ProductPreviewComponent],
})
export class E2ProductPreviewModule {}
