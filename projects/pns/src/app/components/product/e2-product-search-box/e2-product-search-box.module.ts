import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { E2ProductSearchBoxComponent } from './e2-product-search-box.component';

@NgModule({
  declarations: [E2ProductSearchBoxComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    PerfectScrollbarModule,
  ],
  exports: [E2ProductSearchBoxComponent],
})
export class E2ProductSearchBoxModule {}
