import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import {
  ActivatedRoute, Event as NavigationEvent,
  NavigationEnd, Router
} from '@angular/router';
import { Subscription } from 'rxjs';
import { E2SearchService } from '../../../services/e2-search.service';

@Component({
  selector: 'e2-product-search-box',
  templateUrl: './e2-product-search-box.component.html',
  host: {
    '(click)': 'hostInside($event)',
    '(window:click)': 'hostOutside($event)',
  },
  encapsulation: ViewEncapsulation.None,
})
export class E2ProductSearchBoxComponent implements OnInit, OnDestroy {
  @Input() placeholder = '';
  @Output() closeModalEmitter = new EventEmitter<boolean>();
  @Output() searchingEmitter = new EventEmitter<boolean>();

  routerSub$: Subscription;
  isEmpty$ = false;
  isOpened$ = false;
  isSearched$ = false;
  queryParam$: string | null | undefined = '' ;

  constructor(
    private e2SearchService: E2SearchService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.routerSub$ = this.router.events.subscribe((event: NavigationEvent) => {
      if (event instanceof NavigationEnd) {
        this.detectQueryParam();
      }
    });
  }

  ngOnInit(): void {
    this.detectQueryParam();
  }

  ngOnDestroy(): void {
    this.routerSub$?.unsubscribe();
  }

  hostInside(event?: any) {
    event.stopPropagation();
    if ((event.target as HTMLElement).tagName === "INPUT") {
      this.isOpened$ = true;
      this.searchingEmitter.emit(true);
    }
  }

  hostOutside(event?: any) {
    event.stopPropagation();
    this.isOpened$ = false;
    this.searchingEmitter.emit(false);
  }

  onChange(searchValue?: string) {
    if (searchValue !== '') {
      this.isSearched$ = true;
      this.isOpened$ = true;
    } else {
      this.isSearched$ = false;
    }
  }

  searchClear(event?: any, el?: any) {
    event.stopPropagation();
    el.value = '';
    this.isSearched$ = false;
  }

  removeLabel(event?: any) {
    event.stopPropagation();
    event.target.parentNode.parentNode.removeChild(event.target.parentNode);
  }

  searchQuery(queryText?: string, event?: any) {
    if (event) {
      event.stopPropagation();
    }
    this.e2SearchService.searchQueryParams(queryText);
    this.isOpened$ = false;
    this.searchingEmitter.emit(false);
    this.closeModalEmitter.emit(true);
  }

  detectQueryParam() {
    this.queryParam$ = this.e2SearchService.getSearchQueryParams();
    if (this.queryParam$) {
      this.isSearched$ = true;
    }
  }

}
