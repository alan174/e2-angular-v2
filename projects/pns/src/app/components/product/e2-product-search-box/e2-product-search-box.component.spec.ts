import { ComponentFixture, TestBed } from '@angular/core/testing';

import { E2ProductSearchBoxComponent } from './e2-product-search-box.component';

describe('E2ProductSearchBoxComponent', () => {
  let component: E2ProductSearchBoxComponent;
  let fixture: ComponentFixture<E2ProductSearchBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ E2ProductSearchBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(E2ProductSearchBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
