import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SwiperModule } from 'swiper/angular';
import { E2ProductSwiperModule } from '../../other/e2-product-swiper/e2-product-swiper.module';
import { E2ProductCarouselComponent } from './e2-product-carousel.component';

@NgModule({
  declarations: [E2ProductCarouselComponent],
  imports: [CommonModule, E2ProductSwiperModule, SwiperModule],
  exports: [E2ProductCarouselComponent],
})
export class E2ProductCarouselModule {}
