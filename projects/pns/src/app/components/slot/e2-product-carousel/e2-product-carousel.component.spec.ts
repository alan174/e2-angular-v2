import { ComponentFixture, TestBed } from '@angular/core/testing';

import { E2ProductCarouselComponent } from './e2-product-carousel.component';

describe('E2ProductCarouselComponent', () => {
  let component: E2ProductCarouselComponent;
  let fixture: ComponentFixture<E2ProductCarouselComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ E2ProductCarouselComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(E2ProductCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
