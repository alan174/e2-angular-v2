import { ComponentFixture, TestBed } from '@angular/core/testing';

import { E2BrandCarouselComponent } from './e2-brand-carousel.component';

describe('E2BrandCarouselComponent', () => {
  let component: E2BrandCarouselComponent;
  let fixture: ComponentFixture<E2BrandCarouselComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ E2BrandCarouselComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(E2BrandCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
