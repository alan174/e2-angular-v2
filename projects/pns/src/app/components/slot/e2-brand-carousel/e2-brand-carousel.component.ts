import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'e2-brand-carousel',
  templateUrl: './e2-brand-carousel.component.html',
})
export class E2BrandCarouselComponent implements OnInit {
  brandData = [
    {
      name: 'Calbee Promotions',
      brandBanner: 'assets/img/sample/d_banner_brandpromoA.png',
      slides: [
        {
          name: 'Calbee Jagabee',
          unit: '100G',
          productId: 301,
          currentPrice: '20.00',
          originalPrice: '15.00',
          isMember: true,
          hasStock: 'HASSTOCK',
          image: 'assets/img/sample/product_single.png',
        },
        {
          name: 'Calbee Potota Crisps',
          unit: '100G',
          productId: 302,
          currentPrice: '20.00',
          hasStock: 'HASSTOCK',
          image: 'assets/img/sample/product_single.png',
        },
        {
          name: 'Calbee ebi crisps',
          unit: '100G',
          productId: 303,
          currentPrice: '20.00',
          originalPrice: '15.00',
          isMember: true,
          hasStock: 'HASSTOCK',
          image: 'assets/img/sample/product_single.png',
        },
        {
          name: 'Calbee Cheese Crisps',
          unit: '100G',
          productId: 304,
          currentPrice: '20.00',
          hasStock: 'HASSTOCK',
          image: 'assets/img/sample/product_single.png',
        },
        {
          name: 'Calbee Jagabee Calbee Jagabee Calbee Jagabee Calbee Jagabee',
          unit: '100G',
          productId: 305,
          currentPrice: '20.00',
          originalPrice: '15.00',
          isMember: true,
          hasStock: 'HASSTOCK',
          image: 'assets/img/sample/product_single.png',
        },
        {
          name: 'Calbee Jagabee',
          unit: '100G',
          productId: 306,
          currentPrice: '20.00',
          hasStock: 'HASSTOCK',
          image: 'assets/img/sample/product_single.png',
        },
      ],
    },
  ];
  constructor() {}

  ngOnInit(): void {}
}
