import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { E2BrandPromotionModule } from '../../product/e2-brand-promotion/e2-brand-promotion.module';
import { E2BrandCarouselComponent } from './e2-brand-carousel.component';

@NgModule({
  declarations: [E2BrandCarouselComponent],
  imports: [
    CommonModule,
    E2BrandPromotionModule
  ],
  exports: [E2BrandCarouselComponent],
})
export class E2BrandCarouselModule {}
