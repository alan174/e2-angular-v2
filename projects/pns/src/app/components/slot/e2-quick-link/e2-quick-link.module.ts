import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
// import { E2MediaModule } from 'projects/e2-lib/src/public-api';
import { E2QuickLinkComponent } from './e2-quick-link.component';

@NgModule({
  declarations: [E2QuickLinkComponent],
  imports: [
    CommonModule,
    // E2MediaModule
  ],
  exports: [E2QuickLinkComponent],
})
export class E2QuickLinkModule {}
