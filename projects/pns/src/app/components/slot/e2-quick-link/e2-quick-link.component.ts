import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'e2-quick-link',
  templateUrl: './e2-quick-link.component.html',
})
export class E2QuickLinkComponent implements OnInit {

  quicklinkItem = [
    {
      name:'New Arrivals',
      logo:'assets/img/sample/icon_quicklink.png'
    },
    {
      name:'Promotion',
      logo:'assets/img/sample/icon_quicklink.png'
    },
    {
      name:'Flagship Store',
      logo:'assets/img/sample/icon_quicklink.png'
    },
    {
      name:'Shop for gift',
      logo:'assets/img/sample/icon_quicklink.png'
    },
    {
      name:'Sustainability Choices',
      logo:'assets/img/sample/icon_quicklink.png'
    },
    {
      name:'Member Price',
      logo:'assets/img/sample/icon_quicklink.png'
    },
    {
      name:'Exclusive',
      logo:'assets/img/sample/icon_quicklink.png'
    },
    {
      name:'PNS TV',
      logo:'assets/img/sample/icon_quicklink.png'
    },
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
