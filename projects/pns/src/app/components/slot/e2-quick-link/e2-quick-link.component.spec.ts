import { ComponentFixture, TestBed } from '@angular/core/testing';

import { E2QuickLinkComponent } from './e2-quick-link.component';

describe('E2QuickLinkComponent', () => {
  let component: E2QuickLinkComponent;
  let fixture: ComponentFixture<E2QuickLinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ E2QuickLinkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(E2QuickLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
