import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { E2MediaModule } from 'e2-lib';
import { SwiperModule } from 'swiper/angular';
import { E2AddToCartModule } from '../../cart/e2-add-to-cart/e2-add-to-cart.module';
import { E2NotifyMeModule } from '../../cart/e2-notify-me/e2-notify-me.module';
import { E2ViewBundleModule } from '../../cart/e2-view-bundle/e2-view-bundle.module';
import { E2BundlePriceComponent } from './e2-bundle-price.component';

@NgModule({
  declarations: [E2BundlePriceComponent],
  imports: [
    CommonModule,
    SwiperModule,
    E2AddToCartModule,
    E2NotifyMeModule,
    E2ViewBundleModule,
    E2MediaModule,
  ],
  exports: [E2BundlePriceComponent],
})
export class E2BundlePriceModule {}
