import { Component, ElementRef, HostListener } from '@angular/core';
import { SwiperOptions } from 'swiper';

@Component({
  selector: 'e2-bundle-price',
  templateUrl: './e2-bundle-price.component.html',
})
export class E2BundlePriceComponent {
  needFiller;
  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.needFiller = !(
      this.elementRef.nativeElement.closest('body')?.clientWidth < 768
    );
  }
  constructor(private elementRef: ElementRef) {}
  slides = [
    {
      name: 'Japanese Set',
      productId: 307,
      currentPrice: '188.00',
      originalPrice: '212.2',
      hasStock: 'HASSTOCK',
      image: 'assets/img/sample/product_bundle.png',
      isBundle: true,
      items: [
        {
          name: 'Green Tea Ice Cream',
          unit: '100G',
          quantity: '1',
          image: 'assets/img/sample/product_single.png',
        },
        {
          name: 'Strawberry Ice Cream',
          unit: '100G',
          quantity: '1',
          image: 'assets/img/sample/product_single.png',
        },
        {
          name: 'Caramel Biscuit & Cream Ice Cream',
          unit: '200G',
          quantity: '1',
          image: 'assets/img/sample/product_single.png',
        },
      ],
    },
    {
      name: 'Deluxe Meal Set',
      productId: 308,
      currentPrice: '188.00',
      originalPrice: '212.2',
      hasStock: 'HASSTOCK',
      image: 'assets/img/sample/product_bundle.png',
      isBundle: true,
      items: [
        {
          name: 'Green Tea Ice Cream',
          unit: '100G',
          quantity: '1',
          image: 'assets/img/sample/product_single.png',
        },
        {
          name: 'Strawberry Ice Cream',
          unit: '100G',
          quantity: '1',
          image: 'assets/img/sample/product_single.png',
        },
        {
          name: 'Caramel Biscuit & Cream Ice Cream',
          unit: '200G',
          quantity: '1',
          image: 'assets/img/sample/product_single.png',
        },
      ],
    },
    {
      name: 'Korean Set',
      productId: 309,
      currentPrice: '188.00',
      originalPrice: '212.2',
      hasStock: 'HASSTOCK',
      image: 'assets/img/sample/product_bundle.png',
      isBundle: true,
      items: [
        {
          name: 'Green Tea Ice Cream',
          unit: '100G',
          quantity: '1',
          image: 'assets/img/sample/product_single.png',
        },
        {
          name: 'Strawberry Ice Cream',
          unit: '100G',
          quantity: '1',
          image: 'assets/img/sample/product_single.png',
        },
        {
          name: 'Caramel Biscuit & Cream Ice Cream',
          unit: '200G',
          quantity: '1',
          image: 'assets/img/sample/product_single.png',
        },
      ],
    },
    {
      name: 'Ice cream Set',
      productId: 310,
      currentPrice: '188.00',
      originalPrice: '212.2',
      hasStock: 'HASSTOCK',
      image: 'assets/img/sample/product_bundle.png',
      isBundle: true,
      items: [
        {
          name: 'Green Tea Ice Cream',
          unit: '100G',
          quantity: '1',
          image: 'assets/img/sample/product_single.png',
        },
        {
          name: 'Strawberry Ice Cream',
          unit: '100G',
          quantity: '1',
          image: 'assets/img/sample/product_single.png',
        },
        {
          name: 'Caramel Biscuit & Cream Ice Cream',
          unit: '200G',
          quantity: '1',
          image: 'assets/img/sample/product_single.png',
        },
      ],
    },
    {
      name: 'Japanese Set Japanese Set Japanese Set Japanese Set Japanese Set Japanese Set Japanese Set ',
      productId: 311,
      currentPrice: '188.00',
      originalPrice: '212.2',
      hasStock: 'HASSTOCK',
      image: 'assets/img/sample/product_bundle.png',
      isBundle: true,
      items: [
        {
          name: 'Green Tea Ice Cream',
          unit: '100G',
          quantity: '1',
          image: 'assets/img/sample/product_single.png',
        },
        {
          name: 'Strawberry Ice Cream Ice Cream Ice Cream Ice Cream Ice Cream Ice Cream Ice Cream Ice Cream Ice Cream Ice Cream Ice Cream',
          unit: '100G',
          quantity: '1',
          image: 'assets/img/sample/product_single.png',
        },
        {
          name: 'Caramel Biscuit & Cream Ice Cream',
          unit: '200G',
          quantity: '1',
          image: 'assets/img/sample/product_single.png',
        },
      ],
    },
    {
      name: 'Calbee Jagabee',
      productId: 312,
      currentPrice: '188.00',
      originalPrice: '212.2',
      hasStock: 'HASSTOCK',
      image: 'assets/img/sample/product_bundle.png',
      isBundle: true,
      items: [
        {
          name: 'Green Tea Ice Cream',
          unit: '100G',
          quantity: '1',
          image: 'assets/img/sample/product_single.png',
        },
        {
          name: 'Strawberry Ice Cream',
          unit: '100G',
          quantity: '1',
          image: 'assets/img/sample/product_single.png',
        },
        {
          name: 'Caramel Biscuit & Cream Ice Cream',
          unit: '200G',
          quantity: '1',
          image: 'assets/img/sample/product_single.png',
        },
      ],
    },
  ];
  config: SwiperOptions = {
    slidesPerView: 1,
    slidesPerGroup: 1,
    spaceBetween: 5,
    breakpoints: {
      768: {
        slidesPerView: 4,
        slidesPerGroup: 4,
      },
    },
    preventClicks: false,
    preventClicksPropagation: false,
    lazy: {
      loadPrevNext: true,
      loadPrevNextAmount: 3,
    },
    observer: true,
    preventInteractionOnTransition: true,
  };
  ngOnInit(): void {
    this.needFiller = !(
      this.elementRef.nativeElement.closest('body')?.clientWidth < 768
    );
  }
}
