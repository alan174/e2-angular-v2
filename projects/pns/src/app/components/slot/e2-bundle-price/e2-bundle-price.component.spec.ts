import { ComponentFixture, TestBed } from '@angular/core/testing';

import { E2BundlePriceComponent } from './e2-bundle-price.component';

describe('E2BundlePriceComponent', () => {
  let component: E2BundlePriceComponent;
  let fixture: ComponentFixture<E2BundlePriceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ E2BundlePriceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(E2BundlePriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
