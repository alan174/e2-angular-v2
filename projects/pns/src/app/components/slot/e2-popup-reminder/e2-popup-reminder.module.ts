import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { E2PopupReminderComponent } from './e2-popup-reminder.component';

@NgModule({
  declarations: [E2PopupReminderComponent],
  imports: [CommonModule],
  exports: [E2PopupReminderComponent],
})
export class E2PopupReminderModule {}
