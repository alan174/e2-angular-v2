import { ComponentFixture, TestBed } from '@angular/core/testing';

import { E2PopupReminderComponent } from './e2-popup-reminder.component';

describe('E2PopupReminderComponent', () => {
  let component: E2PopupReminderComponent;
  let fixture: ComponentFixture<E2PopupReminderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ E2PopupReminderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(E2PopupReminderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
