import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { E2BannerCarouselModule } from '../../other/e2-banner-carousel/e2-banner-carousel.module';
import { E2MainBannerComponent } from './e2-main-banner.component';

@NgModule({
  declarations: [E2MainBannerComponent],
  imports: [
    CommonModule,
    E2BannerCarouselModule
  ],
  exports: [E2MainBannerComponent],
})
export class  E2MainBannerModule{}
