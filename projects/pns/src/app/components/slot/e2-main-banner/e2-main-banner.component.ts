import { Component, OnInit } from '@angular/core';
import { SwiperOptions } from 'swiper';
@Component({
  selector: 'e2-main-banner',
  templateUrl: './e2-main-banner.component.html',
})
export class E2MainBannerComponent implements OnInit {
  
  slides=[
    'assets/img/sample/banner_hero1.png',
    'assets/img/sample/banner_hero2.png',
    'assets/img/sample/banner_hero3.png',
    'assets/img/sample/banner_hero1.png',
    'assets/img/sample/banner_hero2.png',
    'assets/img/sample/banner_hero3.png',
    'assets/img/sample/banner_hero1.png',
    'assets/img/sample/banner_hero2.png',
  ]
  config: SwiperOptions = {
    autoplay: {
      delay: 7000,
      stopOnLastSlide: false,
      disableOnInteraction: false,
    },
    spaceBetween: 10,
    navigation: {
      prevEl: 'e2-main-banner .previous',
      nextEl: 'e2-main-banner .next',
    },
    pagination: {
      el: 'e2-main-banner .indicators',
      bulletElement: 'span',
      clickable: true,
    },
    loop: true,
    loopFillGroupWithBlank: true,
    loopAdditionalSlides: 1,
    preventClicks: false,
    preventClicksPropagation: false,
    lazy: {
      loadPrevNext: true,
      loadPrevNextAmount: 3,
    },
    observer: true,
    preventInteractionOnTransition: true,
  };
  constructor() { }
  ngOnInit(): void {
  }

}
