import { ComponentFixture, TestBed } from '@angular/core/testing';

import { E2MainBannerComponent } from './e2-main-banner.component';

describe('E2MainBannerComponent', () => {
  let component: E2MainBannerComponent;
  let fixture: ComponentFixture<E2MainBannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ E2MainBannerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(E2MainBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
