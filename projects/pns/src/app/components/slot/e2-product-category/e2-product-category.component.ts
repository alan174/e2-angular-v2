import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SwiperOptions } from 'swiper';
@Component({
  selector: 'e2-product-category',
  templateUrl: './e2-product-category.component.html',
})
export class E2ProductCategoryComponent implements OnInit {
  productCategoryGroup = [];
  config: SwiperOptions = {
    slidesPerView: 'auto',
    slidesPerGroup: 1,
    spaceBetween: 5,
    // preventClicks: false,
    // preventClicksPropagation: false,
    lazy: {
      loadPrevNext: true,
      loadPrevNextAmount: 3,
    },
    observer: true,
    preventInteractionOnTransition: true,
  };

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    switch (this.route.snapshot.data.cssName) {
      case 'e2-promotion-multiple-brand-page':
        this.productCategoryGroup = [
          {
            name: '',
            url: 'assets/img/sample/img_categorybg1.png',
          },
          {
            name: '',
            url: 'assets/img/sample/img_categorybg2.png',
          },
          {
            name: '',
            url: 'assets/img/sample/img_categorybg3.png',
          },
          {
            name: '',
            url: 'assets/img/sample/img_categorybg1.png',
          },
          {
            name: '',
            url: 'assets/img/sample/img_categorybg2.png',
          },
        ];
        break;
      default:
        this.productCategoryGroup = [
          {
            name: 'Dairy, Chilled & Eggs',
            url: 'assets/img/sample/img_categorybg1.png',
          },
          {
            name: 'Fresh Food',
            url: 'assets/img/sample/img_categorybg2.png',
          },
          {
            name: 'Groceries',
            url: 'assets/img/sample/img_categorybg3.png',
          },
          {
            name: 'Frozen Food',
            url: 'assets/img/sample/img_categorybg1.png',
          },
          {
            name: 'Snack',
            url: 'assets/img/sample/img_categorybg2.png',
          },
          {
            name: 'Alcohol',
            url: 'assets/img/sample/img_categorybg3.png',
          },
          {
            name: 'Beverages',
            url: 'assets/img/sample/img_categorybg1.png',
          },
          {
            name: 'Dairy, Chilled & Eggs',
            url: 'assets/img/sample/img_categorybg2.png',
          },
        ];
    }
  }
  formatName(input): string {
    return input.toLowerCase().split(/\W+/).join('-') || '';
  }
}
