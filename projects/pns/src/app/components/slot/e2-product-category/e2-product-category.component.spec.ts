import { ComponentFixture, TestBed } from '@angular/core/testing';

import { E2ProductCategoryComponent } from './e2-product-category.component';

describe('E2ProductCategoryComponent', () => {
  let component: E2ProductCategoryComponent;
  let fixture: ComponentFixture<E2ProductCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ E2ProductCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(E2ProductCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
