import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { E2MediaModule } from 'e2-lib';
import { SwiperModule } from 'swiper/angular';
import { E2ProductCategoryComponent } from './e2-product-category.component';

@NgModule({
  declarations: [E2ProductCategoryComponent],
  imports: [CommonModule, SwiperModule, E2MediaModule, RouterModule],
  exports: [E2ProductCategoryComponent],
})
export class E2ProductCategoryModule {}
