import { ComponentFixture, TestBed } from '@angular/core/testing';

import { E2SuperBrandPromotionComponent } from './e2-super-brand-promotion.component';

describe('E2SuperBrandPromotionComponent', () => {
  let component: E2SuperBrandPromotionComponent;
  let fixture: ComponentFixture<E2SuperBrandPromotionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ E2SuperBrandPromotionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(E2SuperBrandPromotionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
