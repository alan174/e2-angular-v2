import { Component, ElementRef, HostListener, OnInit } from '@angular/core';

@Component({
  selector: 'e2-super-brand-promotion',
  templateUrl: './e2-super-brand-promotion.component.html',
})
export class E2SuperBrandPromotionComponent implements OnInit {
  superBrand = {
    name: 'Old Pot Rice Noodles',
    description:
      'Pure Rice Noodles with Vegetables: Using freshly harvested in-season vegetables with Indica rice to produce rice noodles. Simple and natural materials make delicious food.',
    logo: 'assets/img/sample/logo_oldpot.png',
    mBanner: 'assets/img/sample/m_banner_brandpromoB.png',
    dBanner: 'assets/img/sample/d_banner_brandpromoB.png',
    productList: [
      {
        name: 'OLD POT RICE NOODLES TW RICE-NOODLE SRIR-FRY RICE NDL 4P',
        productId: 31,
        currentPrice: '49.90',
        image: 'assets/img/sample/product_single.png',
        hasStock: 'HASSTOCK',
      },
      {
        name: 'OLD POT RICE NOODLES TW RICE-NOODLE SRIR-FRY RICE NDL 4P',
        productId: 32,
        currentPrice: '49.90',
        image: 'assets/img/sample/product_single.png',
        hasStock: 'HASSTOCK',
      },
      {
        name: 'OLD POT RICE NOODLES TW RICE-NOODLE SRIR-FRY RICE NDL 4P',
        productId: 33,
        currentPrice: '49.90',
        image: 'assets/img/sample/product_single.png',
        hasStock: 'HASSTOCK',
      },
    ],
  };
  isMobile;
  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.isMobile = !(
      this.elementRef.nativeElement.closest('body')?.clientWidth > 940
    );
  }
  constructor(private elementRef: ElementRef) {}

  ngOnInit(): void {
    this.isMobile = !(
      this.elementRef.nativeElement.closest('body')?.clientWidth > 940
    );
  }
}
