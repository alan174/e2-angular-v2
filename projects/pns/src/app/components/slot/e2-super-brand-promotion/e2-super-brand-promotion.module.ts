import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { E2MediaModule } from 'e2-lib';
import { E2AddToCartModule } from '../../cart/e2-add-to-cart/e2-add-to-cart.module';
import { E2NotifyMeModule } from '../../cart/e2-notify-me/e2-notify-me.module';
import { E2SuperBrandPromotionComponent } from './e2-super-brand-promotion.component';

@NgModule({
  declarations: [E2SuperBrandPromotionComponent],
  imports: [
    CommonModule,
    E2AddToCartModule,
    E2NotifyMeModule,
    E2MediaModule,
    RouterModule,
  ],
  exports: [E2SuperBrandPromotionComponent],
})
export class E2SuperBrandPromotionModule {}
