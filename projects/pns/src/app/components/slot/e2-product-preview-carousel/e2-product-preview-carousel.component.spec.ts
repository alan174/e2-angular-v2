import { ComponentFixture, TestBed } from '@angular/core/testing';

import { E2ProductPreviewCarouselComponent } from './e2-product-preview-carousel.component';

describe('E2ProductPreviewCarouselComponent', () => {
  let component: E2ProductPreviewCarouselComponent;
  let fixture: ComponentFixture<E2ProductPreviewCarouselComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ E2ProductPreviewCarouselComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(E2ProductPreviewCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
