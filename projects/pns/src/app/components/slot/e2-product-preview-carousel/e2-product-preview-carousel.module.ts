import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { E2ProductPreviewModule } from '../../product/e2-product-preview/e2-product-preview.module';
import { E2ProductPreviewCarouselComponent } from './e2-product-preview-carousel.component';

@NgModule({
  declarations: [E2ProductPreviewCarouselComponent],
  imports: [CommonModule, E2ProductPreviewModule],
  exports: [E2ProductPreviewCarouselComponent],
})
export class E2ProductPreviewCarouselModule {}
