import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'e2-product-preview-carousel',
  templateUrl: './e2-product-preview-carousel.component.html',
})
export class E2ProductPreviewCarouselComponent implements OnInit {
  twolist = [
    {
      name: 'Member Price',
      item: [
        {
          image: 'assets/img/sample/product_single.png',
          productId: 101,
          currentPrice: '130.00',
          isMember: true,
          hasStock: 'HASSTOCK',
        },
        {
          image: 'assets/img/sample/product_single.png',
          productId: 102,
          currentPrice: '130.00',
          isMember: true,
          hasStock: 'HASSTOCK',
        },
        {
          image: 'assets/img/sample/product_single.png',
          productId: 103,
          currentPrice: '130.00',
          isMember: true,
          hasStock: 'HASSTOCK',
        },
        {
          image: 'assets/img/sample/product_single.png',
          productId: 104,
          currentPrice: '130.00',
          isMember: true,
          hasStock: 'HASSTOCK',
        },
      ],
    },
    {
      name: 'Buy again',
      item: [
        {
          image: 'assets/img/sample/product_single.png',
          productId: 105,
          currentPrice: '130.00',
          hasStock: 'HASSTOCK',
        },
        {
          image: 'assets/img/sample/product_single.png',
          productId: 106,
          currentPrice: '130.00',
          hasStock: 'HASSTOCK',
        },
        {
          image: 'assets/img/sample/product_single.png',
          productId: 107,
          currentPrice: '130.00',
          isMember: true,
          hasStock: 'HASSTOCK',
        },
        {
          image: 'assets/img/sample/product_single.png',
          productId: 108,
          currentPrice: '130.00',
          hasStock: 'HASSTOCK',
        },
      ],
    },
  ];
  constructor() {}

  ngOnInit(): void {}
}
