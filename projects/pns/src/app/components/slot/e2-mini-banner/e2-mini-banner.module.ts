import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { E2MediaModule } from 'e2-lib';
import { SwiperModule } from 'swiper/angular';
import { E2MiniBannerComponent } from './e2-mini-banner.component';

@NgModule({
  declarations: [E2MiniBannerComponent],
  imports: [
    CommonModule,
    SwiperModule,
    E2MediaModule
  ],
  exports: [E2MiniBannerComponent],
})
export class  E2MiniBannerModule{}
