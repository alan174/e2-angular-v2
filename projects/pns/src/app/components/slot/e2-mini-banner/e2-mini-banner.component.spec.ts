import { ComponentFixture, TestBed } from '@angular/core/testing';

import { E2MiniBannerComponent } from './e2-mini-banner.component';

describe('E2MiniBannerComponent', () => {
  let component: E2MiniBannerComponent;
  let fixture: ComponentFixture<E2MiniBannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ E2MiniBannerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(E2MiniBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
