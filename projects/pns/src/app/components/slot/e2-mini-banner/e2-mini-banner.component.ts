import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SwiperOptions } from 'swiper';

@Component({
  selector: 'e2-mini-banner',
  templateUrl: './e2-mini-banner.component.html',
})
export class E2MiniBannerComponent implements OnInit {
  slides;
  config: SwiperOptions = {
    slidesPerView: 'auto',
    spaceBetween: 5,
    preventClicks: false,
    preventClicksPropagation: false,
    lazy: {
      loadPrevNext: true,
      loadPrevNextAmount: 3,
    },
    observer: true,
    preventInteractionOnTransition: true,
  };
  constructor(private route: ActivatedRoute) {}
  ngOnInit(): void {
    switch (this.route.snapshot.data.cssName) {
      case 'e2-promotion-multiple-brand-page':
        this.slides = [
          'assets/img/sample/banner_secondary1.png',
          'assets/img/sample/banner_secondary2.png',
        ];
        break;
      default:
        this.slides = [
          'assets/img/sample/banner_secondary1.png',
          'assets/img/sample/banner_secondary2.png',
          'assets/img/sample/banner_secondary3.png',
          'assets/img/sample/banner_secondary1.png',
          'assets/img/sample/banner_secondary2.png',
        ];
    }
  }
}
