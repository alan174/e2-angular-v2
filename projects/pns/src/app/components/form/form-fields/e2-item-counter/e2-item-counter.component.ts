import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'e2-item-counter',
  templateUrl: './e2-item-counter.component.html',
})
export class E2ItemCounterComponent implements OnInit {
    @Input() min = 0;
    @Input() max: any;
    @Input() val: any;
    @Input() errorMsg: any;
    @Input() unavailableMsg: any;
    @Input() noEdit = false;
    @Input() disabled = false;
    @Input() unavailable = false;

    inputVal = 0;
  constructor(
  ) { }

  ngOnInit(): void {
    this.inputVal = this.val || this.min;
  }
  minusVal(e: any):void{
    if(!(this.inputVal<= this.min)){
      this.inputVal--
      e.target.closest('.input-container').lastElementChild.classList.remove('disabled');
    }
    if(this.inputVal<= this.min){
      e.target.parentElement.classList.add("disabled")
    }
  }
  plusVal(e: any):void{
    if(!(this.inputVal>= this.max)){
      this.inputVal++
      e.target.closest('.input-container').firstElementChild.classList.remove('disabled');
    }
    if(this.inputVal>= this.max){
      e.target.parentElement.classList.add("disabled")
    }
  }
}
