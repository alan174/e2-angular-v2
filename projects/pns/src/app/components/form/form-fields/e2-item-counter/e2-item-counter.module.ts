import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { E2ItemCounterComponent } from './e2-item-counter.component';

@NgModule({
  declarations: [E2ItemCounterComponent],
  imports: [CommonModule],
  exports: [E2ItemCounterComponent],
})
export class E2ItemCounterModule {}
