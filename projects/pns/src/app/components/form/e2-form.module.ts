import { NgModule } from '@angular/core';
import { E2ItemCounterModule } from './form-fields/e2-item-counter/e2-item-counter.module';

@NgModule({
  imports: [
    E2ItemCounterModule
  ],
  exports: [
    E2ItemCounterModule
  ],
  declarations: []
})
export class E2FormModule { }
