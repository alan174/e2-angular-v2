import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'e2-search-box',
  templateUrl: './e2-search-box.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class E2SearchBoxComponent implements OnInit {
  @Input() placeholder: string;

  constructor() {}

  ngOnInit(): void {}

  searchClear(el: HTMLInputElement) {
    el.value = '';
  }
}
