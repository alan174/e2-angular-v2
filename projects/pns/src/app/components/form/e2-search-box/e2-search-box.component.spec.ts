import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { E2SearchBoxComponent } from './e2-search-box.component';

describe('E2SearchBoxComponent', () => {
  let component: E2SearchBoxComponent;
  let fixture: ComponentFixture<E2SearchBoxComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ E2SearchBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(E2SearchBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
