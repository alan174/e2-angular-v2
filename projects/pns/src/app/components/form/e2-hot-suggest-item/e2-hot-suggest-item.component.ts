import { Component, OnInit } from '@angular/core';
import { E2SearchService } from '../../../services/e2-search.service';

@Component({
  selector: 'e2-hot-suggest-item',
  templateUrl: './e2-hot-suggest-item.component.html',
})
export class E2HotSuggestItemComponent implements OnInit {

  constructor(
    private e2SearchService: E2SearchService,
  ) { }

  ngOnInit(): void {
  }

  searchQuery(queryText?: string, event?: any) {
    if (event) {
      event.stopPropagation();
    }
    this.e2SearchService.searchQueryParams(queryText);
  }

}
