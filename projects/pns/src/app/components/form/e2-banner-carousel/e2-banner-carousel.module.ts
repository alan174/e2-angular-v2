import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { E2MediaModule } from 'e2-lib';
import { SwiperModule } from 'swiper/angular';
import { E2BannerCarouselComponent } from './e2-banner-carousel.component';

@NgModule({
  declarations: [E2BannerCarouselComponent],
  imports: [
    CommonModule,
    SwiperModule,
    E2MediaModule
  ],
  exports: [E2BannerCarouselComponent],
})
export class E2BannerCarouselModule {}
