import { ChangeDetectorRef, Component, Input } from '@angular/core';
import SwiperCore, { Autoplay, Navigation, Pagination } from "swiper";

SwiperCore.use([Autoplay, Pagination, Navigation]);
@Component({
  selector: 'e2-banner-carousel',
  templateUrl: './e2-banner-carousel.component.html',
})
export class E2BannerCarouselComponent{
  @Input() config;
  @Input() slides;
  fade = false;
  
  constructor(private cdr: ChangeDetectorRef) { }

  toogleHide(flag):void{
    this.fade = flag;
    this.cdr.detectChanges();
  }
}
