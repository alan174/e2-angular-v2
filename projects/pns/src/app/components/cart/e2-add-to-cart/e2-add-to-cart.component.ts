import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'e2-add-to-cart',
  templateUrl: './e2-add-to-cart.component.html',
})
export class E2AddToCartComponent implements OnInit {
  @Input() compact = false;
  @Input() iconOnly = false;
  @Input() islarge = false;
  @Input() isBundle = false;
  @Input() isFreeGift = false;
  @Input() stockStatus = '';
  @Input() product: any;
  demoShowItemCounter = false;
  hasStock$ = new BehaviorSubject<boolean>(false);
  constructor() {}

  ngOnInit(): void {
    this.hasStock$.next(this.stockStatus !== 'OUTOFSTOCK');
  }
}
