import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { E2ItemCounterModule } from '../../form/form-fields/e2-item-counter/e2-item-counter.module';
import { E2AddToCartComponent } from './e2-add-to-cart.component';

@NgModule({
  declarations: [E2AddToCartComponent],
  imports: [CommonModule, E2ItemCounterModule],
  exports: [E2AddToCartComponent],
})
export class E2AddToCartModule {}
