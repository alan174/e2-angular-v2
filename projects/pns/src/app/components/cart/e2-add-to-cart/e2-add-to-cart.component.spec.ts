import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { E2AddToCartComponent } from './e2-add-to-cart.component';

describe('E2AddToCartComponent', () => {
  let component: E2AddToCartComponent;
  let fixture: ComponentFixture<E2AddToCartComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ E2AddToCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(E2AddToCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
