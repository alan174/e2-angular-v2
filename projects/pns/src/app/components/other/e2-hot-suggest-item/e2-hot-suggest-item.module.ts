import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { E2CommonModule } from 'e2-lib';
import { E2HotSuggestItemComponent } from './e2-hot-suggest-item.component';

@NgModule({
  declarations: [E2HotSuggestItemComponent],
  imports: [
    CommonModule,
    E2CommonModule,
  ],
  exports: [E2HotSuggestItemComponent],
})
export class E2HotSuggestItemModule {}
