import { ComponentFixture, TestBed } from '@angular/core/testing';

import { E2HotSuggestItemComponent } from './e2-hot-suggest-item.component';

describe('E2HotSuggestItemComponent', () => {
  let component: E2HotSuggestItemComponent;
  let fixture: ComponentFixture<E2HotSuggestItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ E2HotSuggestItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(E2HotSuggestItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
