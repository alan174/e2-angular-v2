import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { E2SearchBoxComponent } from './e2-search-box.component';

@NgModule({
  declarations: [E2SearchBoxComponent],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
  ],
  exports: [E2SearchBoxComponent],
})
export class E2SearchBoxModule {}
