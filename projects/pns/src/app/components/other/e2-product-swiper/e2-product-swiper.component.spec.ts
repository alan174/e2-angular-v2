import { ComponentFixture, TestBed } from '@angular/core/testing';

import { E2ProductSwiperComponent } from './e2-product-swiper.component';

describe('E2ProductSwiperComponent', () => {
  let component: E2ProductSwiperComponent;
  let fixture: ComponentFixture<E2ProductSwiperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ E2ProductSwiperComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(E2ProductSwiperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
