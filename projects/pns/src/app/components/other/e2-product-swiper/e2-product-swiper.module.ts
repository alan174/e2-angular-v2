import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SwiperModule } from 'swiper/angular';
import { E2ProductTileModule } from '../../product/e2-product-tile/e2-product-tile.module';
import { E2ProductSwiperComponent } from './e2-product-swiper.component';

@NgModule({
  declarations: [E2ProductSwiperComponent],
  imports: [CommonModule, SwiperModule, E2ProductTileModule, RouterModule],
  exports: [E2ProductSwiperComponent],
})
export class E2ProductSwiperModule {}
