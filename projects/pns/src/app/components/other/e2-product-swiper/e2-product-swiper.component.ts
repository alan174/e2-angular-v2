import {
  Component,
  ElementRef,
  HostListener,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import SwiperCore, { Navigation, Pagination, SwiperOptions } from 'swiper';

SwiperCore.use([Pagination, Navigation]);
@Component({
  selector: 'e2-product-swiper',
  templateUrl: './e2-product-swiper.component.html',
})
export class E2ProductSwiperComponent implements OnInit {
  @Input() item;
  sectionClassName;
  hideSwiper = false;
  constructor(private elementRef: ElementRef) {}

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.checkHideSwiper();
  }
  config: SwiperOptions = {
    slidesPerView: 'auto',
    spaceBetween: 6,
    slidesOffsetBefore: 5,
    // preventClicks: false,
    // preventClicksPropagation: false,
    lazy: {
      loadPrevNext: true,
      loadPrevNextAmount: 3,
    },
    observer: true,
    preventInteractionOnTransition: true,
  };

  @ViewChild('swiper') swiper;
  ngOnInit(): void {
    this.checkHideSwiper();
    if (this.item?.hasPageBtn) {
      this.sectionClassName =
        this.item.title?.toLowerCase().split(/\W+/).join('-') || '';
      this.config = {
        ...this.config,
        navigation: {
          prevEl: 'e2-product-swiper .' + this.sectionClassName + ' .previous',
          nextEl: 'e2-product-swiper .' + this.sectionClassName + ' .next',
        },
        pagination: {
          el: 'e2-product-swiper .' + this.sectionClassName + ' .pageNumber',
          type: 'fraction',
        },
      };
      this.swiper?.update();
    }
  }
  checkHideSwiper(): void {
    if (this.item?.mobileNoSwiper) {
      this.hideSwiper = !(
        this.elementRef.nativeElement.closest('body')?.clientWidth > 767
      );
    }
  }
}
