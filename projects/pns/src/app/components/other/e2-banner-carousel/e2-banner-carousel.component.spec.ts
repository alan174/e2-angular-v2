import { ComponentFixture, TestBed } from '@angular/core/testing';

import { E2BannerCarouselComponent } from './e2-banner-carousel.component';

describe('E2BannerCarouselComponent', () => {
  let component: E2BannerCarouselComponent;
  let fixture: ComponentFixture<E2BannerCarouselComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ E2BannerCarouselComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(E2BannerCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
