import { LayoutConfig } from '@spartacus/storefront';
import { E2HeaderConfig } from '../header/e2-header.config';
import { E2HomepageTemplateConfig } from './e2-home-page/e2-homepage-template.config';
import { E2FooterConfig } from '../footer/e2-footer.config';

export function pageLayoutConfig(): LayoutConfig {
  return {
    layoutSlots: {
      ...E2HeaderConfig,
      ...E2HomepageTemplateConfig,
      ...E2FooterConfig
    }
  }
}
