export const E2HomepageTemplateConfig = {
  e2HomepageTemplate: {
    slots: [
      'MainBanner',
      'PopupReminder',
      'QuickLink',
      'ProductPreview',
      'BuyAgain',
      'PromotionMiniBanner',
      'MerchantStore',
      'BrandPromotion',
      'BundlePrice',
      'ProductCarousel',
      'CaseOffer',
      'SuperBrandPromotion',
      'ProductCategory',
    ],
  }
}
