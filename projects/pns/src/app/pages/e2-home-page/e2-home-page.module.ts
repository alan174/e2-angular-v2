import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SwiperModule } from 'swiper/angular';
import { RouterModule } from '@angular/router';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { E2AddToCartModule } from '../../components/cart/e2-add-to-cart/e2-add-to-cart.module';
import { E2BrandCarouselModule } from '../../components/slot/e2-brand-carousel/e2-brand-carousel.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SwiperModule,
    PerfectScrollbarModule,
    RouterModule,
    E2AddToCartModule,
    E2BrandCarouselModule
  ],
  providers: [],
})
export class E2HomePageModule {}
