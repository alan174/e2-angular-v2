import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterBottomComponent } from './footer-bottom/footer-bottom.component';
import { FooterLinksComponent } from './footer-links/footer-links.component';
import { FooterMobileAppComponent } from './footer-mobile-app/footer-mobile-app.component';
import { FooterPSPComponent } from './footer-psp/footer-psp.component';


@NgModule({
  declarations: [
    FooterBottomComponent,
    FooterLinksComponent,
    FooterMobileAppComponent,
    FooterPSPComponent
  ],
  imports: [
    CommonModule,
  ],
  providers: [],
})
export class FooterModule {}
