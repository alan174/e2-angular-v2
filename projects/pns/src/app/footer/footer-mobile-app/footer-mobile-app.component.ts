import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-footer-mobile-app',
  templateUrl: './footer-mobile-app.component.html',
  encapsulation: ViewEncapsulation.None
})
export class FooterMobileAppComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
