import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-footer-bottom',
  templateUrl: './footer-bottom.component.html',
  encapsulation: ViewEncapsulation.None
})
export class FooterBottomComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
