export const E2FooterConfig = {
  footer: {
    slots: [
      'FooterPSP',
      'FooterMobileApp',
      // 'FooterLinks',
      'FooterBottom',
    ],
  }
}
