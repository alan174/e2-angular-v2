import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-footer-links',
  templateUrl: './footer-links.component.html',
  encapsulation: ViewEncapsulation.None
})
export class FooterLinksComponent implements OnInit {

  public hideRuleContent: boolean[] = [];
  public buttonName: any = 'Expand';

  constructor() { }

  ngOnInit(): void {
  }

  toggle(index: number) {
    // toggle based on index
    this.hideRuleContent[index] = !this.hideRuleContent[index];
  }

}
