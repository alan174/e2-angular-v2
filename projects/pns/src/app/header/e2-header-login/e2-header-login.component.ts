import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'e2-header-login',
  templateUrl: './e2-header-login.component.html',
  host: {
    '(mouseover)': 'hostInside($event)',
    '(mouseleave)': 'hostOutside($event)',
  },
  encapsulation: ViewEncapsulation.None,
})
export class E2HeaderLoginComponent implements OnInit {
  isOpened$ = false;

  constructor() { }

  ngOnInit(): void {
  }

  hostInside(event: any) {
    this.isOpened$ = true;
  }

  hostOutside(event: any) {
    this.isOpened$ = false;
  }

}
