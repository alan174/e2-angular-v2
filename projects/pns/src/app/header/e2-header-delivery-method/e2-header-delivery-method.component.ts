import { Component, ElementRef, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'e2-header-delivery-method',
  templateUrl: './e2-header-delivery-method.component.html',
})
export class E2HeaderDeliveryMethodComponent implements OnInit {
  form: any;
  step: any;
  signedIn = false;
  activeVal = 'hd';
  tempAddress: any;
  pickedDelivery: any;
  deliveryAddress: any;
  items = [
    {
      name: 'Home Delivery',
      value: 'hd',
      icon: 'delivery',
      img: 'assets/img/share/illustration_HomeD_',
      banner: 'assets/img/share/banner_HomeD.png',
      checked: true,
    },
    {
      name: 'Click & Collect',
      value: 'cnc',
      icon: 'cnc',
      img: 'assets/img/share/illustration_CNC_',
      banner: 'assets/img/share/banner_CNC.png',
      checked: false,
    },
  ];
  regionList = [
    { name: 'Region', value: '' },
    { name: 'Hong Kong Island', value: 'HK' },
    { name: 'Kowloon', value: 'KLN' },
    { name: 'New Territories', value: 'NT' },
  ];
  districtList = [{ name: 'District', value: '' }];
  storeList = [
    {
      radioText: {
        name: 'AC1 (Aberdeen Centre)',
        logo: 'assets/img/share/icon_discount.png',
        address:
          'Unit 5, 26/F, Paul Y. Centre, 51 Hung To Road, Kwun Tong Kwun Tong Kowloon',
        officeHour: '0800 - 2230',
      },
      default: true,
      value: '1',
      checked: true,
    },
    {
      radioText: {
        name: 'Hilton Tower',
        logo: 'assets/img/share/logo_pns_square.png',
        address:
          'G/F, Hilton Towers, 96 Granville Road, Tsim Sha Tsui, Kowloon',
        officeHour: '0800 - 2230',
      },
      value: '2',
      checked: false,
    },
    {
      radioText: {
        name: 'Marina Square',
        logo: 'assets/img/share/logo_pns_square.png',
        address:
          'G/F, Hilton Towers, 96 Granville Road, Tsim Sha Tsui, Kowloon',
        officeHour: '0800 - 2230',
      },
      value: '3',
      checked: false,
    },
    {
      radioText: {
        name: 'Walton Estate SS',
        logo: 'assets/img/share/logo_pns_square.png',
        address:
          'G/F, Hilton Towers, 96 Granville Road, Tsim Sha Tsui, Kowloon',
        officeHour: '0800 - 2230',
      },
      value: '4',
      checked: false,
    },
    {
      radioText: {
        name: 'Hilton Tower',
        logo: 'assets/img/share/logo_pns_square.png',
        address:
          'G/F, Hilton Towers, 96 Granville Road, Tsim Sha Tsui, Kowloon',
        officeHour: '0800 - 2230',
      },
      value: '5',
      checked: false,
    },
  ];
  constructor(private modalService: NgbModal, private elementRef: ElementRef) {}

  ngOnInit(): void {
    this.confirmDelivery();
  }
  ngAfterViewChecked(): void {
    var modal = this.elementRef.nativeElement
      .closest('body')
      .querySelector('.delivery-method-modal .modal-content');
    if (modal) {
      modal.style.setProperty('--100vh', window.innerHeight + 'px');
    }
  }

  openModal(content: any, className: any): void {
    this.step = 0;
    this.activeVal = this.pickedDelivery;
    this.modalService.open(content, {
      backdrop: 'static',
      // keyboard: false,
      centered: true,
      windowClass: className,
    });
  }

  pickStoreAddress(e: any): void {
    this.tempAddress = e;
  }

  confirmDelivery(): void {
    this.pickedDelivery = this.activeVal;
    switch (this.activeVal) {
      case 'hd':
        this.pickedDelivery = 'hd';
        break;
      case 'cnc':
        this.pickedDelivery = 'cnc';
        this.deliveryAddress = this.tempAddress;
        break;
      default:
    }
  }
}
