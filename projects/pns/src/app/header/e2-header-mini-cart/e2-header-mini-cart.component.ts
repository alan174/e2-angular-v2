import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'e2-header-mini-cart',
  templateUrl: './e2-header-mini-cart.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class E2HeaderMiniCartComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
