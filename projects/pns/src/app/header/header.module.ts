import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { E2HeaderDeliveryMethodComponent } from './e2-header-delivery-method/e2-header-delivery-method.component';
import { E2HeaderLoginComponent } from './e2-header-login/e2-header-login.component';
import { E2HeaderMiniCartComponent } from './e2-header-mini-cart/e2-header-mini-cart.component';
import { E2HeaderNavigationBarComponent } from './e2-header-navigation-bar/e2-header-navigation-bar.component';
import { E2HeaderNavigationTabComponent } from './e2-header-navigation-tab/e2-header-navigation-tab.component';
import { E2HeaderNotificationComponent } from './e2-header-notification/e2-header-notification.component';
import { E2HeaderSearchBoxComponent } from './e2-header-search-box/e2-header-search-box.component';
import { E2HeaderSiteLanguageComponent } from './e2-header-site-language/e2-header-site-language.component';
import { E2SiteContextSelectorComponent } from './e2-site-context-selector/e2-site-context-selector.component';
import { RouterModule } from '@angular/router';
import { SwiperModule } from 'swiper/angular';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { E2FormModule } from '../components/form/e2-form.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { E2ProductSearchBoxModule } from '../components/product/e2-product-search-box/e2-product-search-box.module';
import { ConfigModule, NotAuthGuard } from '@spartacus/core';
import { BannerComponent, LinkComponent } from '@spartacus/storefront';
import { E2SearchBoxComponent } from '../components/other/e2-search-box/e2-search-box.component';


@NgModule({
  declarations: [
    E2HeaderDeliveryMethodComponent,
    E2HeaderLoginComponent,
    E2HeaderMiniCartComponent,
    E2HeaderNavigationBarComponent,
    E2HeaderNavigationTabComponent,
    E2HeaderNotificationComponent,
    E2HeaderSearchBoxComponent,
    E2HeaderSiteLanguageComponent,
    E2SiteContextSelectorComponent
  ],
  imports: [
    CommonModule,
    SwiperModule,
    PerfectScrollbarModule,
    RouterModule,
    E2FormModule,
    FormsModule,
    ReactiveFormsModule,
    E2ProductSearchBoxModule,
    ConfigModule.withConfig({
      cmsComponents: {
        E2MiniCartComponent: { component: E2HeaderMiniCartComponent },
        CMSSiteContextComponent: {
          component: E2SiteContextSelectorComponent
        },
        E2NotificationComponent: { component: E2HeaderNotificationComponent },
        E2SearchBoxComponent: { component: E2SearchBoxComponent },
        E2DeliveryMethodComponent: { component: E2HeaderDeliveryMethodComponent },
        E2LoginComponent: {
          component: E2HeaderLoginComponent,
          // for redirect to previous route after login success
          guards: [NotAuthGuard],
        },
        E2NavigationBarComponent: { component: E2HeaderNavigationBarComponent },
      },
    }),
  ],
  providers: [],
})
export class HeaderModule {}
