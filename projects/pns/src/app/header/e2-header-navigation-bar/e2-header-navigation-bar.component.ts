import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, HostListener, Inject, OnDestroy, OnInit } from '@angular/core';
import {
  Event as NavigationEvent,
  NavigationStart,
  Router
} from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'e2-header-navigation-bar',
  templateUrl: './e2-header-navigation-bar.component.html',
})
export class E2HeaderNavigationBarComponent implements OnInit, OnDestroy {
  @HostListener('window:scroll', ['$event'])
  onScroll(event: any) {
    const wp$ = event.target['scrollingElement'].scrollTop;
    const header$ = this.elementRef.nativeElement.closest('header');

    if (wp$ >= header$?.querySelector('.header')?.offsetHeight) {
      header$?.classList.add('headerMini');
    } else {
      header$?.classList.remove('headerMini');
    }


    console.log()
    //getElementsByClassName('link')
//this.document.body?.querySelectorAll('.e2ProductDetailsPageTemplate .ProductTabs')


   /*  const el$ = this.elementRef.nativeElement;
    const rectTop$ = el$.getBoundingClientRect().top;
   // console.log((rectTop$ + el$.offsetHeight))

    //console.log(document.documentElement.scrollTop)

    if ((rectTop$ + el$.offsetHeight) <= 0) {
      el$.closest('header')?.classList.add('headerMini');
    } else {

     el$.closest('header')?.classList.remove('headerMini');
    } */
  }

  routerSub$: Subscription;
  isNavOpen$: boolean = false;
  routerUrl$: string | undefined;
  data$: any[] = [
    {
      linkName: 'Food Market',
      link: '/',
      lv2: [
        {
          linkName: 'Alcohol',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
        {
          linkName: 'Beverages',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Beverages',
            },
          ],
        },
        {
          linkName: 'Food Catering',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Beverages',
            },
          ],
        },
        {
          linkName: 'Grocery',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Beverages',
            },
          ],
        },
        {
          linkName: 'Snacks',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Beverages',
            },
          ],
        },
        {
          linkName: 'Frozen Food',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Beverages',
            },
          ],
        },
        {
          linkName: 'Fresh Vegetable & Fruit',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Beverages',
            },
          ],
        },
        {
          linkName: 'Breakfast & Jam',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Beverages',
            },
          ],
        },
        {
          linkName: 'Dairy, Soy Product & Eggs',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Beverages',
            },
          ],
        },
      ],
    },
    {
      linkName: 'Baby & Mum',
      link: '/login',
      lv2: [
        {
          linkName: 'Baby & Mum',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
        {
          linkName: 'Beverages',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Beverages',
            },
          ],
        },
        {
          linkName: 'Food Catering',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
        {
          linkName: 'Grocery',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
        {
          linkName: 'Snacks',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
        {
          linkName: 'Frozen Food',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
        {
          linkName: 'Fresh Vegetable & Fruit',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
        {
          linkName: 'Breakfast & Jam',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
        {
          linkName: 'Dairy, Soy Product & Eggs',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
      ],
    },
    {
      linkName: 'Personal Care & Health',
      link: '',
      lv2: [
        {
          linkName: 'Personal Care & Health',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
        {
          linkName: 'Beverages',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Beverages',
            },
          ],
        },
        {
          linkName: 'Food Catering',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Beverages',
            },
          ],
        },
        {
          linkName: 'Grocery',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Beverages',
            },
          ],
        },
        {
          linkName: 'Snacks',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Beverages',
            },
          ],
        },
        {
          linkName: 'Frozen Food',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Beverages',
            },
          ],
        },
        {
          linkName: 'Fresh Vegetable & Fruit',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Beverages',
            },
          ],
        },
        {
          linkName: 'Breakfast & Jam',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Beverages',
            },
          ],
        },
        {
          linkName: 'Dairy, Soy Product & Eggs',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Beverages',
            },
          ],
        },
      ],
    },
    {
      linkName: 'Home & Entertainment',
      link: '',
      lv2: [
        {
          linkName: 'Home & Entertainment',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
        {
          linkName: 'Beverages',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Beverages',
            },
          ],
        },
        {
          linkName: 'Food Catering',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
        {
          linkName: 'Grocery',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
        {
          linkName: 'Snacks',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
        {
          linkName: 'Frozen Food',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
        {
          linkName: 'Fresh Vegetable & Fruit',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
        {
          linkName: 'Breakfast & Jam',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
        {
          linkName: 'Dairy, Soy Product & Eggs',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
      ],
    },
    {
      linkName: 'Pets',
      link: '',
      lv2: [
        {
          linkName: 'Cat',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
        {
          linkName: 'Dog',
          link: '',
          lv3: [
            {
              linkName: 'lv3 Alcohol',
            },
          ],
        },
      ],
    },
    {
      linkName: 'Featured Items',
      link: '',
      className: 'navFeaturedItems',
    },
    {
      linkName: 'Clearance Sales',
      link: '',
      className: 'navClearanceSales',
    },
    {
      linkName: 'Demo',
      link: '/demo',
      className: 'navDemo',
    },
  ];

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private elementRef: ElementRef,
    private router: Router
  ) {
    this.routerSub$ = this.router.events.subscribe((event: NavigationEvent) => {
      if (event instanceof NavigationStart) {
        this.routerUrl$ = event?.url;
        if (this.routerUrl$ == '/') {
          this.elementRef.nativeElement
            .closest('e2-header-navigation-bar')
            ?.classList.add('navOpen');
        } else {
          this.elementRef.nativeElement
            .closest('e2-header-navigation-bar')
            ?.classList.remove('navOpen');
        }
      }
    });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.routerSub$?.unsubscribe();
  }

  setSelected(linkUrl: string) {
    if (this.routerUrl$ == linkUrl) {
      return true;
    } else {
      return false;
    }
  }

  setNavOpen(linkUrl: string) {
    if (this.routerUrl$ == linkUrl && this.routerUrl$ == '/') {
      return true;
    } else {
      return false;
    }
  }

}
