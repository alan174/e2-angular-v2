import {
  Component,
  ElementRef,
  HostListener,
  Input,
  OnInit
} from '@angular/core';

@Component({
  selector: 'e2-header-navigation-tab',
  templateUrl: './e2-header-navigation-tab.component.html',
})
export class E2HeaderNavigationTabComponent implements OnInit {
  @Input('componentData') componentData$: any;
  @Input('isNavOpen') isNavOpen$: any;
  @Input('isSelected') isSelected$: any;

  @HostListener('mouseover', ['$event'])
  onMouseOver(event: any) {
    event.stopPropagation();
    clearTimeout(this.isTimeout$);
    this.isTimeout$ = setTimeout(() => {
      if (
        event.target?.matches('.nav-item') &&
        !event.target?.matches('.noSubMenu') &&
        !event.target?.matches('.navOpened')
      ) {
        event.target.classList.add('active');
        this.elementRef.nativeElement
          .closest('e2-header-navigation-bar')
          ?.classList.add('activeOpen');
      }
    }, 500);
  }
  @HostListener('mouseleave', ['$event'])
  onMouseleave(event: any) {
    event.stopPropagation();
    clearTimeout(this.isTimeout$);
    this.isNavSubOpen$ = false;
    event.target.firstChild.classList?.remove('active');
    if (
      event.target.firstChild?.matches('.nav-item') &&
      !event.target.firstChild?.matches('.noSubMenu') &&
      !event.target.firstChild?.matches('.navOpened')
    ) {
      this.elementRef.nativeElement
        .closest('e2-header-navigation-bar')
        ?.classList.remove('activeOpen');
    }
  }

  isNavSubOpen$ = false;
  isSubOpened$: any = {};
  isTimeout$: any;

  constructor(private elementRef: ElementRef) {}

  ngOnInit() {}

  subMenuActive(active?: any, subMenuList?: any) {
    if (subMenuList != null) {
      this.isNavSubOpen$ = true;
    }
    return active;
  }
}
