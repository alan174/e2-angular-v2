export const E2HeaderConfig = {
  header: {
    slots: [
      'TopBanner',
      'PromotionText',
      'Notification',
      'SiteLanguage',
      'SiteLogo',
      'SearchBox',
      'HotSuggestItem',
      'DeliveryMethod',
      'MiniCart',
      'SiteLogin',
      'NavigationBar'
    ],
    lg: {
      slots: [
        'TopBanner',
        'PromotionText',
        'Notification',
        'SiteLanguage',
        'SiteLogo',
        'SearchBox',
        'HotSuggestItem',
        'DeliveryMethod',
        'MiniCart',
        'SiteLogin',
        'NavigationBar'
      ],
    },
  }
}
