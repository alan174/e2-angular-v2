import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-e2-header-notification',
  templateUrl: './e2-header-notification.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class E2HeaderNotificationComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
