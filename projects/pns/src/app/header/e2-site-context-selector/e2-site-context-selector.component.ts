import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-e2-site-context-selector',
  templateUrl: './e2-site-context-selector.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class E2SiteContextSelectorComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
