import { HttpClientModule } from '@angular/common/http';
import { Injector, NgModule } from '@angular/core';
import {
  BrowserModule,
  BrowserTransferStateModule,
} from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SpartacusModule } from './spartacus/spartacus.module';
import { E2HomePageModule } from './pages/e2-home-page/e2-home-page.module';
import {
  BannerCarouselComponent,
  BannerComponent,
  LinkComponent, NavigationModule,
  PageSlotModule,
} from '@spartacus/storefront';
import { ConfigModule, NotAuthGuard } from '@spartacus/core';
import { HeaderModule } from './header/header.module';
import { FooterModule } from './footer/footer.module';
import { pageLayoutConfig } from './pages/page-layout.config';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([]),
    SpartacusModule,
    BrowserTransferStateModule,
    PageSlotModule,
    ConfigModule.withConfig({
      breakpoints: {
        xs: 1024,
        md: 1024,
        lg: 1024,
      },
      skipLinks: [],
      cmsComponents: {
        E2CMSLinkComponent: { component: LinkComponent },
        E2BannerComponent: { component: BannerComponent },
      },
    }),
    E2HomePageModule,
    NavigationModule,
    HeaderModule,
    FooterModule,
    ConfigModule.withConfigFactory(pageLayoutConfig),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
